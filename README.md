# Simple Story creator

Version: **Alpha 0.1**

This is a simple character creator, for 99,999% of the world is usless, but if
someone wants it, you are free to take it

If you are going to use it I hope you will like it :)

## Installing

To install SCC, you'll need to clone the repository using ``git clone
https://gitlab.com/DactusTheCactus/simple-story-creator``. After doing so,
please see the next section: *Building*.

## Building

Simple Character Creator is usually compiled with **g++**.

You can use the "build.sh" script to compile and or install it. For more info,
run ``./build.sh`` after making sure you are in the right folder.

##

    Copyright (C) 2021 DactusTheCactus
    <gitlab.com/DactusTheCactus>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License   
    along with this program.  If not, see <https://www.gnu.org/licenses/>.      
