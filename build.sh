#!/usr/bin/env sh

# SETTINGS ########

# preferences
rootcmd="sudo" # program used to obtain root privileges for installation
compiler="g++" # C++ compiler

# folders
installDir="/usr/local/bin/" # folder in which SCC will get installed

###################

# MAIN VARS #######

errormsg="Compilation failed! Check GCC output for errors."

###################

helpMe() {
    printf "\nScript for compiling and installing Simple Story Creator.

Usage:  ./build.sh [options]

Options:
  compile           creates binary file of the program and places it
  in the ./bin dir
  install           installs the program to the chosen directory (see
  script's 'SETTINGS' section)

[Author: DactusTheCactus <gitlab.com/DactusTheCactus>]\n\n"
}

folderCheck() {
    case $PWD in
        *simple-story-creator*|*simple-character-creator*) :;; # if folder doesn't contain these words, then
        *) printf "\nPlease make sure you're in the
        right folder before running this script. Thanks!\n\n";
           exit 100 # exit with error status
    esac
}

sccCompile() {
    folderCheck
    $compiler -o bin/scc main.cpp
}

sccInstall() {
    folderCheck
    if ! sccCompile; then printf "\n%s\n\n", "$errormsg"; fi
    $rootcmd cp bin/scc "$installDir"
}

# FLAGS ###########

case $1 in
    compile) if ! sccCompile; then printf "\n%s\n\n", "$errormsg"; fi;;
    install) if ! sccInstall; then printf "\n%s\n\n", "$errormsg"; fi;;
    *)       helpMe
esac

###################
